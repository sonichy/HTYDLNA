# HTYDLNA
A mini DLNA sender, can not receive yet.  
![alt](preview.png)

## Test
| Soft | Pass | Status |
|:----:|:----:|:----:|
| [Macast](https://github.com/xfangfang/Macast) | ✓ | Fast |
| [乐播](https://www.lebo.cn) | X | SDK |
| Kodi | X | Occasionally|

## Debug Tool
sudo ngrep port xxxx

## Local Server
python3 -m http.server

## Reference
https://blog.csdn.net/sinat_33859977/article/details/113567847  
https://github.com/sYCH3L/TwitchTVDLNAPlayer
